process blastp {

  label 'blast_container'
  label 'blastp'
  tag "${dataset}/${pat_name}"

  input:
  tuple val(pat_name), val(dataset), path(blastp_fasta)
  path data_dir
  val parstr
  val species

  output:
  tuple val(pat_name), val(dataset), path("*blastp.out"), emit: blastp_hits

  script:
  """
  export BLASTDB=$data_dir
  blastp -query ${blastp_fasta} -db  ${species}.bdb ${parstr}> ${dataset}-${pat_name}.blastp.out
  """
}
